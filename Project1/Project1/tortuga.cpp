#include <glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>

#define M_PI 3.14159265358979323846
using namespace std;

float rotate_y=0;
float rotate_x=0;
float rotate_z=0;

GLfloat X = 0.0f;
GLfloat Y = 0.0f;
GLfloat Z = 0.0f;
float scale = 1.0f;

bool bx=false;
bool by=false;
bool bz=false;

void init(void) {
glClearColor(0.0, 0.0, 0.0, 0.0);
}

void ArrowKey(int key, int x, int y) {
//C�digo
switch(key){
case GLUT_KEY_RIGHT:
X+=1.0;
break;
//  Flecha izquierda: disminuir rotaci�n 5 grados
case GLUT_KEY_LEFT:
X-= 1.0;
break;
case GLUT_KEY_UP:
Y+= 1.0;
break;
case GLUT_KEY_DOWN:
Y-= 1.0;
break;
case GLUT_KEY_HOME:
scale+=0.5;
break;
case GLUT_KEY_END:
scale+=-0.5;
break;
}

glutPostRedisplay();
}

void dibujaCubo(){

//LADO FRONTAL: lado multicolor
glBegin(GL_POLYGON);
glColor3f( 1.0, 0.0, 0.0 );     glVertex3f(  0.5, -0.5, -0.5 );      // P1 es rojo
//glColor3f( 0.0, 1.0, 0.0 );    
glVertex3f(  0.5,  0.5, -0.5 );      // P2 es verde
//glColor3f( 0.0, 0.0, 1.0 );
    glVertex3f( -0.5,  0.5, -0.5 );      // P3 es azul
//glColor3f( 1.0, 0.0, 1.0 );  
glVertex3f( -0.5, -0.5, -0.5 );      // P4 es morado
glEnd();
// LADO TRASERO: lado amarillo
glBegin(GL_POLYGON);
glColor3f(   1.0,  1.0, 0.0 );
glVertex3f(  0.5, -0.5, 0.5 );
glVertex3f(  0.5,  0.5, 0.5 );
glVertex3f( -0.5,  0.5, 0.5 );
glVertex3f( -0.5, -0.5, 0.5 );
glEnd();
// LADO DERECHO: lado morado
glBegin(GL_TRIANGLES);
glColor3f(  1.0,  0.0,  1.0 );
glVertex3f( 1.5, -0.5, -0.5 );
glVertex3f( 1.5,  0.5, -0.5 );
glVertex3f( 1.5,  0.5,  0.5 );

glEnd();


}

void display(void) {
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
glLoadIdentity();
gluLookAt(0.0-X, 0.0-Y, 4.5-Z, 0.0-X, 0.0-Y, 0.0-Z, 0.0, 1.0, 0.0);//(0.0,0.0,0.5,0.0,0.0,0.0,0.0,1.0,0.0);
glPushMatrix();
if(bx==true){
rotate_x += .2f;
}
if(by==true){
rotate_y += .2f;
}
if(bz==true){
rotate_z += .2f;
}

glRotatef( rotate_x, 1.0, 0.0, 0.0 );
glRotatef( rotate_y, 0.0, 1.0, 0.0 );
glRotatef( rotate_z, 0.0, 0.0, 1.0 );

glScalef(scale, scale, scale);
dibujaCubo();

glPopMatrix();
glFlush();
glutSwapBuffers();
}

void reshape(int w, int h) {
glViewport(0, 0, (GLsizei)w, (GLsizei)h);
glMatrixMode(GL_PROJECTION);
glLoadIdentity();
glOrtho(-10.0, 10.0, -10.00, 10.0, 0.1, 20.0);
glMatrixMode(GL_MODELVIEW);

}

void keyboard(unsigned char key, int x, int y) {
switch (key)
{

case 43:
Z+=1.0;
break;
case 45:
Z-=1.0;
break;
case 120:
if(by==false){
by=true;
}
else{
by=false;
}
break;
case 121:
if(bx==false){
bx=true;
}
else{
bx=false;
}
break;
case 122:
if(bz==false){
bz=true;
}
else{
bz=false;
}
break;    
case 27:
exit(0);
break;
}
}
